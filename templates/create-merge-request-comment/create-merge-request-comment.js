const axios = require('axios');

const gitlabApiUrl = process.argv[2];
const projectId = process.env.CI_MERGE_REQUEST_PROJECT_ID;
const mergeRequestId = process.env.CI_MERGE_REQUEST_IID;
const accessToken = process.env.ACCESS_TOKEN; // Read access token from CI variable

const commentText = process.argv[3];
console.log('<< access token',process.env.CI_MERGE_REQUEST_PROJECT_ID, ':id:', process.env.CI_MERGE_REQUEST_IID);
axios({
  method: 'post',
  url: `${gitlabApiUrl}/projects/${projectId}/merge_requests/${mergeRequestId}/notes`,
  headers: {
    'Private-Token': accessToken,
  },
  data: {
    body: commentText,
  },
})
  .then(response => {
    console.log('Comment added successfully:', response.data);
  })
  .catch(error => {
    console.error('Error adding comment:', error.response);
  });
